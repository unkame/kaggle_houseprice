'''
Kaggle - House price
Feature engineered (same as model.py) & use cv models
Best submission: 0.12711
'''

import os, sys
import numpy as np, pandas as pd
import re
from collections import Counter
from pathlib import Path
from scipy.stats import skew
from scipy.special import boxcox

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import mean_squared_error, mean_squared_log_error
from sklearn.linear_model import RidgeCV, LassoCV, ElasticNetCV
from sklearn.ensemble import RandomForestRegressor, VotingRegressor, GradientBoostingRegressor, \
                                IsolationForest
from sklearn.model_selection import RandomizedSearchCV

from lightgbm import LGBMRegressor
from xgboost.sklearn import XGBRegressor
from mlxtend.regressor import StackingRegressor


def detect_outliers(df, n, features):
    '''
    Remove outliers by IQR (quantile 1st - 3rd)
    '''
    outlier_indices=[]

    for col in features:
        if col in df.columns.tolist():
            Q1 = np.percentile(df[col],25)
            Q3 = np.percentile(df[col],75)
            IQR = Q3 - Q1
            outlier_list_col = df[(df[col] < Q1 - 1.5 * IQR)|(df[col] > Q3 + 1.5 * IQR)].index
            outlier_indices.extend(outlier_list_col)

    outlier_indices = Counter(outlier_indices)
    multiple_outliers = list(k for k,v in outlier_indices.items() if v>n)
    return multiple_outliers


def modify_features(df):
    '''
    modify dataset by changing features, based on exploration
    (result obtained from houseprice_explore.ipynb)
    '''

    # remove columns 
    ignore_ls = []
    # many 0s, more than 80%
    ignore_ls = ignore_ls + ['PoolArea', '3SsnPorch', 'LowQualFinSF', 'MiscVal', \
                        'BsmtHalfBath', 'ScreenPorch', 'BsmtFinSF2', 'EnclosedPorch']
    # many na, more than 80%
    ignore_ls = ignore_ls + ['PoolQC', 'MiscFeature', 'Alley', 'Fence']
    # high skewness (>3)
    ignore_ls = ignore_ls + ['LotArea', 'KitchenAbvGr']
    ignore_ls = ignore_ls + ['Utilities', 'Street', 'Condition2', 'RoofMatl', \
                        'Heating', 'LandSlope', 'CentralAir', 'Functional', 'GarageCond']
    df = df.drop(ignore_ls, axis=1)

    # special columns
    # convert value to text
    text_ls = ['MSSubClass', 'MoSold']
    for col in text_ls:
        df[col] = df[col].astype(str)

    # number features (except year): fillna by mean
    # year: fillna by median 
    num_ls = list(df.select_dtypes(['int64','float64']))
    year_ls = ['YearBuilt', 'YearRemodAdd', 'GarageYrBlt', 'YrSold']
    for col in num_ls:
        if col not in year_ls:
            df[col].fillna(df[col].mean(), inplace=True)
            #df[col].fillna(0, inplace=True)
        else:
            df[col].fillna(df[col].median(), inplace=True)

    # year handling
    df['House_year'] = df['YrSold'] - df['YearBuilt']
    df['Remod_year'] = df['YrSold'] - df['YearRemodAdd']
    df['Garage_built'] = df['YrSold'] - df['GarageYrBlt']
    #df = df.drop(year_ls, axis=1)              # drop columns make it worse

    # textual features: none
    str_ls = list(df.select_dtypes(['object']))
    for col in str_ls:
        df[col].fillna('none', inplace=True)

    # boxcox for highly skewed number features
    skew_ls = ['MasVnrArea', 'OpenPorchSF']
    for col in skew_ls:
        #df[col]= np.log1p(df[col].dropna())
        df[col]= boxcox(df[col].dropna(), 0.5)

    # onehot (DictVectorizer)
    onehot_ls = list(df.select_dtypes(['object']))
    for col in onehot_ls:
        df[col] = df[col].astype(str)

    dv = DictVectorizer(sparse=False, dtype=int)
    onehot_dict = df[onehot_ls].to_dict(orient='records')
    encode_np = dv.fit_transform(onehot_dict)
    encode_order = dv.get_feature_names()
    encode_df = pd.DataFrame(encode_np, columns=encode_order)
    df = pd.concat([df, encode_df], axis=1)
    df = df.drop(onehot_ls, axis=1)

    return df

def get_error(model, X, y):
    y_pred = model.predict(X)
    return np.sqrt(mean_squared_error(y, y_pred))

def get_error_log(model, X, y):
    y_pred = model.predict(X)
    return np.sqrt(mean_squared_log_error(y, y_pred))

def get_csv(result, col_id, fname="model"):
    y_pred = pd.DataFrame(col_id)
    y_pred['SalePrice'] = pd.DataFrame(data=result, columns=['SalePrice'])
    y_pred.to_csv("model/hp_" + fname + '.csv', sep=',', index=False)


if __name__ == "__main__":
    '''
    Straight foward coding, no object model/class 
    lazy :)
    '''

    br = '\n'
    Path("model/").mkdir(parents=True, exist_ok=True)

    # Read house price data
    # train: 1460, pred: 1459
    data_train = pd.read_csv('data/train.csv')
    data_pred = pd.read_csv('data/test.csv')

    # modify features
    data_all = pd.concat((data_train, data_pred), sort=False).reset_index(drop=True)
    data_all.drop(['Id', 'SalePrice'], axis=1, inplace=True)
    #print(data_all.columns)

    data_all_tfm = modify_features(data_all)
    #print(data_all_tfm.columns)

    # remove outliers of train data
    X = data_all_tfm[:1460]
    y = data_train['SalePrice']
    #print(X.shape, y.shape)

    X_pred = data_all_tfm[1460:]
    y_pred_id = data_pred['Id']

    outlier_ls = list(X.select_dtypes(['int64','float64']))
    outliers = detect_outliers(X, 2, outlier_ls)
    X = X.drop(outliers, axis=0)
    y = y.drop(outliers, axis=0)
    #print(X.shape, y.shape)

    #iso = IsolationForest(contamination=0.1)   # lower performance
    #filters = iso.fit_predict(X[outlier_ls])
    #filters = filters != -1
    #X = X.iloc[filters, :]
    #y = y.iloc[filters]
    #print(X.shape, y.shape)

    # start training
    # 1. use RandomForestRegressor, RidgeCV, LassoCV, ElasticNetCV,
    #       LGBMRegressor, XGBRegressor
    # 2. then apply the estimators in voting & stacking
    # 3. then apply voting & stacking by blending

    # y = np.log1p(y)   # lower performance
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

    # RandomForestRegressor, not used as performance not good
    '''
    rfr = RandomForestRegressor
    rfr_params = { 'n_estimators': [100, 500], 
                    'bootstrap': [True, False], 
                    'max_features': ['auto', 'sqrt', None],
                    'min_samples_split': [2, 5, 10, 15, 100],
                    'min_samples_leaf': [1, 2, 5, 10],
                    'max_depth': [1,5,10]
                }  

    random = RandomizedSearchCV(rfr(), param_distributions=rfr_params, 
                                    cv=3, n_jobs=-1, verbose=0, random_state=0)
    random.fit(X_train, y_train)
    bp = random.best_params_
    name = rfr().__class__.__name__
    rfr_tuned = rfr(**bp).fit(X_train, y_train)
    rmse_train, rmse_train_log = get_error(rfr_tuned, X_train, y_train)
    rmse_test, rmse_test_log = get_error(rfr_tuned, X_test, y_test)
    print(name, ":")
    print("train rmse: ", rmse_train, rmse_train_log)
    print("test rmse: ", rmse_test, rmse_test_log)

    #sys.exit()
    '''

    # RidgeCV
    alpha=[0.0001,0.001,0.01,0.1,1,10,100]
    rcv = RidgeCV(cv=5, alphas=alpha, normalize=True)
    rcv_fit = rcv.fit(X_train, y_train)
    rmse_train = get_error(rcv_fit, X_train, y_train)
    rmse_test = get_error(rcv_fit, X_test, y_test)
    rmse_train_log = get_error_log(rcv_fit, X_train, y_train)
    rmse_test_log = get_error_log(rcv_fit, X_test, y_test)
    name = rcv_fit.__class__.__name__
    print(name, ":")
    print("train rmse: ", rmse_train, rmse_train_log)
    print("test rmse: ", rmse_test, rmse_test_log)

    # LassoCV
    #alpha=[0.0001,0.001,0.01,0.1,1,10,100]
    lcv = LassoCV(cv=5, max_iter=1e7, alphas=alpha, normalize=True)
    lcv_fit = lcv.fit(X_train,y_train)
    rmse_train = get_error(lcv_fit, X_train, y_train)
    rmse_test = get_error(lcv_fit, X_test, y_test)
    rmse_train_log = get_error_log(lcv_fit, X_train, y_train)
    rmse_test_log = get_error_log(lcv_fit, X_test, y_test)
    name = lcv_fit.__class__.__name__
    print(name, ":")
    print("train rmse: ", rmse_train, rmse_train_log)
    print("test rmse: ", rmse_test, rmse_test_log)

    # ElasticNetCV
    #alpha = [0.0001,0.001,0.01,0.1,1,10,100]
    l1_ratio = [0.1, 0.5, 0.9, 0.95, 0.99, 1]
    encv = ElasticNetCV(cv=5, max_iter=1e7, alphas=alpha, l1_ratio=l1_ratio, normalize=True)
    encv_fit = encv.fit(X_train, y_train)
    rmse_train = get_error(encv_fit, X_train, y_train)
    rmse_test = get_error(encv_fit, X_test, y_test)
    rmse_train_log = get_error_log(encv_fit, X_train, y_train)
    rmse_test_log = get_error_log(encv_fit, X_test, y_test)
    name = encv_fit.__class__.__name__
    print(name, ":")
    print("train rmse: ", rmse_train, rmse_train_log)
    print("test rmse: ", rmse_test, rmse_test_log)

    # LGBMRegressor
    # used saved hyperparameters for time saving
    lgbm = LGBMRegressor
    parameter_grid = { 'max_depth':range(2,5,1),
                       'learning_rate':np.linspace(0.001,1,20),
                       'colsample_bytree':np.linspace(0.5,0.99,20),
                       'subsample':np.linspace(0.1,0.99,20),
                       'subsample_freq':range(5,10,1),
                       'num_leaves':range(100,200,5),
                       'min_child_samples':range(50,200,10),
                       'n_estimators':range(100,5000,100)
                    }
    
    file_bp = "lgbm_bp.npy"
    if os.path.exists(file_bp):
        bp = np.load(file_bp, allow_pickle=True)
        bp = bp.tolist()
    else:
        random = RandomizedSearchCV(lgbm(),
                                    parameter_grid,
                                    cv = 5,
                                    scoring = 'neg_root_mean_squared_error',
                                    n_iter=500,
                                    n_jobs = -1)
        random.fit(X_train, y_train)
        bp = random.best_params_
        np.save(file_bp, bp)

    lgbm_tuned = lgbm(**bp).fit(X_train, y_train)
    rmse_train = get_error(lgbm_tuned, X_train, y_train)
    rmse_test = get_error(lgbm_tuned, X_test, y_test)
    rmse_train_log = get_error_log(lgbm_tuned, X_train, y_train)
    rmse_test_log = get_error_log(lgbm_tuned, X_test, y_test)
    name = lgbm_tuned.__class__.__name__
    print(name, ":")
    print(bp)
    print("train rmse: ", rmse_train, rmse_train_log)
    print("test rmse: ", rmse_test, rmse_test_log)

    # XGBRegressor
    # used saved hyperparameters for time saving
    # This RandomizedSearchCV needs very very long time
    xgb = XGBRegressor
    parameter_grid = {
        'max_depth':range(2,5,1),
        'learning_rate':np.linspace(0.001,1,20),
        'colsample_bytree':np.linspace(0.1,0.99,20),
        'n_estimators':range(100,5000,100),
    }
    
    file_bp = "xgb_bp.npy"
    if os.path.exists(file_bp):
        bp = np.load(file_bp, allow_pickle=True)
        bp = bp.tolist()
    else:
        random = RandomizedSearchCV(xgb(),
                            parameter_grid,
                            cv = 5,
                            scoring = 'neg_root_mean_squared_error',
                            n_iter=500,
                            n_jobs = -1)
        random.fit(X_train, y_train)
        bp = random.best_params_
        np.save(file_bp, bp)

    xgb_tuned = xgb(**bp).fit(X_train, y_train)
    rmse_train = get_error(xgb_tuned, X_train, y_train)
    rmse_test = get_error(xgb_tuned, X_test, y_test)
    rmse_train_log = get_error_log(xgb_tuned, X_train, y_train)
    rmse_test_log = get_error_log(xgb_tuned, X_test, y_test)
    name = xgb_tuned.__class__.__name__
    print(name, ":")
    print(bp)
    print("train rmse: ", rmse_train, rmse_train_log)
    print("test rmse: ", rmse_test, rmse_test_log)

    # Voting
    vote = VotingRegressor([#('Rfr', rfr_tuned),
                            ('Ridge', rcv_fit), 
                            ('Lasso', lcv_fit),
                            ('Elastic', encv_fit), 
                            ('lgbm', lgbm_tuned),
                            ('xgb',xgb_tuned)
                        ])
    
    vote_fit = vote.fit(X_train, y_train)
    rmse_train = get_error(vote_fit, X_train, y_train)
    rmse_test = get_error(vote_fit, X_test, y_test)
    rmse_train_log = get_error_log(vote_fit, X_train, y_train)
    rmse_test_log = get_error_log(vote_fit, X_test, y_test)
    name = vote_fit.__class__.__name__
    print(name, ":")
    print("train rmse: ", rmse_train, rmse_train_log)
    print("test rmse: ", rmse_test, rmse_test_log)

    # Stacking
    gdbt = GradientBoostingRegressor(learning_rate=0.05, max_leaf_nodes=3,
                                    n_estimators=100)

    stack = StackingRegressor(regressors=[rcv_fit, lcv_fit, encv_fit, lgbm_tuned, xgb_tuned], 
                               meta_regressor=gdbt, 
                               use_features_in_secondary=True)

    stack_fit = stack.fit(X_train, y_train)
    rmse_train = get_error(stack_fit, X_train, y_train)
    rmse_test = get_error(stack_fit, X_test, y_test)
    rmse_train_log = get_error_log(stack_fit, X_train, y_train)
    rmse_test_log = get_error_log(stack_fit, X_test, y_test)
    name = stack_fit.__class__.__name__
    print(name, ":")
    print("train rmse: ", rmse_train, rmse_train_log)
    print("test rmse: ", rmse_test, rmse_test_log)

    # Blending
    weight = list(np.linspace(0.1,1,91))
    train_mse = []
    test_mse = []
    vote_train_pred = vote_fit.predict(X_train)
    vote_test_pred = vote_fit.predict(X_test)

    stack_train_pred = stack_fit.predict(X_train)
    stack_test_pred = stack_fit.predict(X_test)

    # Find out best blending ratio (train part for reference only; use test)
    for i in weight:
        blend_pred_train = (i*vote_train_pred)+((1-i)*stack_train_pred)
        blend_pred_test = (i*vote_test_pred)+((1-i)*stack_test_pred)
        train_mse.append(np.sqrt(mean_squared_log_error(y_train, blend_pred_train)))
        test_mse.append(np.sqrt(mean_squared_log_error(y_test, blend_pred_test)))

    blend_output = pd.DataFrame({'weight':weight,
                                  'train_mse':train_mse,
                                  'test_mse':test_mse})
    
    print("Blending:")
    print(blend_output.sort_values(by = ['test_mse'],ascending = True).head(5))
    prop = blend_output.sort_values(by = ['test_mse'],ascending = True).iloc[0]["weight"]


    # Submission
    # blending by vote + stack
    y_pred_vote = vote_fit.predict(X_pred)
    y_pred_stack = stack_fit.predict(X_pred)
    y_pred_blend = prop*y_pred_vote + (1-prop) * y_pred_stack

    # y_pred_blend = np.expm1(exy_pred_blend)
    get_csv(y_pred_blend, y_pred_id, "blending")

