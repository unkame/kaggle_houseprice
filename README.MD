# Machine learning practice

## Topic: Kaggle - House price 

## Description:
- library: scikit-learn, lightgbm, xgboost
- method: 
    1. explore
    2. build model: multiple regressors > voting + stacking > blending > result
- best result: 0.12711
- possible improvement: 
    1. Further feature engineering (e.g. combination of features, log fr skewed, use log y instead, etc.) 
    2. Grid search cv (high hardware requirements)
    3. Build custom estimators (advanced usage of sklearn.base)

## Reference:
- https://medium.com/@permoonzz/kaggle-house-prices-advanced-regression-techniques-python-ensemble-learning%E5%AF%A6%E5%81%9A-99f757f4d326
